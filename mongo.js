// db.users.insertOne({
// 
//     
//     "username": "dahyunTwice",
// 
//     "password": "dakyunKim"
// 
// 
// })

// db.users.insertOne ({
// 
//     "username": "gokuSon",
// 
//     "password": "over9000"
//     
// })

// insert multiple documents at once 
// db.users.insertMany(
//     [
//         {
//             "username": "pablo123",
//             "password": "123paul"
//         },
//         {
//             "username": "pedro99",
//             "password": "iampeter99"
//         }
//     ]
// )

// db.products.insertMany(
//     [
//         {
//             "name": "chicken",
//             "description": "1pc chicken with rice",
//             "price": 70
//         },
//         {
//             "name": "burger",
//             "description": "regular burger",
//             "price": 30
//         },
//         {
//             "name": "fries",
//             "description": "regular fries",
//             "price": 25
//         }
//      ]
//  )

// db.users.find() - allows us to return/find all documents in the collection
// db.users.find()

//  db.users.find({"criteria":"value"}) - returns/find all documents that match the criteria
// db.users.find({"username": "pedro99"})

// db.cars.insertMany(
//     [
//         {
//             "name": "Vios",
//             "brand": "Toyota",
//             "type": "Sedan",
//             "price": 850000
//         },
//         {
//             "name": "City",
//             "brand": "Honda",
//             "type": "Sedan",
//             "price": 1000000
//         },
//         {
//             "name": "Almera",
//             "brand": "Nissan",
//             "type": "Sedan",
//             "price": 900000
//         }
//     ]
// )

// db.cars.find({"brand":"Honda"})
// db.cars.findOne({}) - find/return the first item/document in the collection
// db.cars.findOne({"criteria":"value"}) - find/return the first item/document that matches the criteria

// db.users.updateOne({"username":"pedro99"},{$set:{"username":"peter1999"}})

// db.collection.updateOne({},{$set:{"fieldToBeUpdated":"Updated Value"}}) - allows us to update the first item in the collection
// db.users.updateOne({},{$set:{"username":"updatedUsername"}})

db.users.updateOne({"username":"pablo123"},{$set:{"isAdmin":true}})
